@if(!empty($Store_data))
    <header class="page-header centeralign">
        <h1><span>@if($store_count!=0) @if(!empty($StoreName)) {{ $StoreName }} @else ALL @endif STORES @php if($store_count!=0) echo "($store_count)"; @endphp @endif</span></h1>
    </header>
    <div class="storesdetail">
        
        @foreach($Store_data as $key => $value)
        <div class="storedetails">
            <h3>{{ $value['store_name'] }}, {{ $value['store_city'] }}</h3>
            <h5>Address</h5>
            <p>{{ $value['store_address'] }}</p>
            <h5>Phone No:</h5>
            {{ $value['store_phone_number'] }}
            <p>{{ $value['store_phone_number'] }}</p>
            <a href="{{ $detail_url }}/{{ strtolower(str_replace(' ', '', $value['store_state'])) }}/{{ strtolower(str_replace(' ', '', $value['store_city'])) }}/{{ $value['store_slug'] }}" class="moredetails"><b>MORE DETAILS > </b></a>   
            @if(!empty($value['whatsapp_number']) && WHATSAPP_DISPLAY != 'false')
            <a href="{{ REDIRECT_WHATSAPP }}{{ $value['whatsapp_number'] }}" class="moredetails"><b>VIRTUAL SHOPPING > </b></a>
            @endif
        </div>
        @endforeach
    </div>
    @if($Store_data instanceof \Illuminate\Pagination\LengthAwarePaginator)
    <div class="paginationlinks">
        {{ $Store_data->links() }}
    </div>
    @endif
@endif