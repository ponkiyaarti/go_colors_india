
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                @if(isset($order_data['customer']))
                <div class="col-md-6">
                    <section class="hk-sec-wrapper">
                        <h5>Customer Information</h5><hr>  
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Name</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['first_name'].' '.$order_data['customer']['last_name'] ?><label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['email'] ?><label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Order Date</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['created_at'] ?><label>
                            </div>
                        </div>
                        
                    </section>
                </div>
                @endif
                @if(!empty($shipping_address))
                <div class="col-md-6">
                    <section class="hk-sec-wrapper">
                        <h5>Shipping Address Information</h5><hr>  
                        
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Address</label>
                            <div class="col-sm-8">
                              <label class="col-form-label">
                                @if(!empty($shipping_address['address1']))
                                <?=$shipping_address['address1'] ?>,
                                @endif
                                
                                @if(!empty($shipping_address['address2']))
                                <?=$shipping_address['address2'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['city']))
                                <?=$shipping_address['city'] ?>,
                                @endif

                                @if(!empty($shipping_address['province']))
                                <?=$shipping_address['province'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['country']))
                                <?=$shipping_address['country'] ?>,
                                @endif
                                @if(!empty($shipping_address['zip']))
                                <?=$shipping_address['zip'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['phone']))
                                <?=$shipping_address['phone'] ?>,
                                @endif

                              </label>
                            </div>
                        </div>
                        
                    </section>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>