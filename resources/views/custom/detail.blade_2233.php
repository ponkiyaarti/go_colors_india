<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state) && !empty($city) && !empty($slug))
    <title>Westside Store in {{ $slug }} - Westside</title>
    <meta name="description" content="Use our store finder to locate Westside stores in {{ $slug }}. Know more about the Westside {{ $slug }} store timings, address, directions & contact number.">
    @else
    <title>storelocator</title>
    @endif

    <link href="//cdn.shopify.com/s/files/1/0266/6276/4597/t/49/assets/vendor.min.css?v=11283820616272568804"
        rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0266/6276/4597/t/49/assets/theme-styles.scss.css?v=4000088586524479405"
        rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/zudio-storelocatore.css">
</head>
<body>
    <div class="container" id="storelocatorpage">
        <div class="breadcrumb">
            <a href="/" data-translate="general.breadcrumbs.home">Home</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $detail_url }}s?type={{$type}}">Store Locator</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $detail_url }}s/{{ \Request::segment(3) }}?type={{$type}}">{{ \Request::segment(3) }}</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $detail_url }}s/{{ \Request::segment(3) }}/{{ \Request::segment(4) }}?type={{$type}}">{{ \Request::segment(4) }}</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <span>{{ \Request::segment(5) }}</span>
        </div>
        @if(!empty($desktop_store_gallery))
        <div class="storegallery desktop_image">
            @foreach($desktop_store_gallery as $value)
            <div>
                <img src="{{ IMAGES_PATH }}{{ $value['images'] }}">
            </div>
            @endforeach
        </div>
        @endif
        @if(!empty($mobile_store_gallery))
        <div class="storegallery mobile_image">
            @foreach($mobile_store_gallery as $value)
            <div>
                <img src="{{ IMAGES_PATH }}{{ $value['images'] }}">
            </div>
            @endforeach
        </div>
        @endif
        <div class="storecontentgallery">
            <div class="storecontent">
                <h1>{{ $StoreMaster_data['store_name'] }}, {{ $StoreMaster_data['store_city'] }}</h1>
                @if(!empty($StoreMaster_data['store_address']))
                <span class="storedata">
                    <b>ADDRESS:</b>
                    <i>{{ $StoreMaster_data['store_address'] }}</i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['store_mobile_number']))
                <span class="storedata">
                    <b>PHONE:</b>
                    <i><a
                            href="tel:{{ $StoreMaster_data['store_mobile_number'] }}">{{ $StoreMaster_data['store_mobile_number'] }}</a></i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['whatsapp_number']))
                <span class="storedata">
                    <b>Whatsapp Number:</b>
                    <i><a
                            href="tel:{{ $StoreMaster_data['whatsapp_number'] }}">{{ $StoreMaster_data['whatsapp_number'] }}</a></i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['monday_time']) || !empty($StoreMaster_data['tuesday_time']) ||
                !empty($StoreMaster_data['wednesday_time']) || !empty($StoreMaster_data['thursday_time']) ||
                !empty($StoreMaster_data['friday_time']) || !empty($StoreMaster_data['saturday_time']) ||
                !empty($StoreMaster_data['sunday_time']))
                <span class="storedata">
                    <b>OPENING HOURS:</b>
                    <i>
                        Monday - @if($StoreMaster_data['monday_open'] == 'Yes'){{ $StoreMaster_data['monday_time'] }}@else Closed @endif<br>
                        Tuesday - @if($StoreMaster_data['tuesday_open'] == 'Yes'){{ $StoreMaster_data['tuesday_time'] }}@else Closed @endif <br>
                        Wednesday - @if($StoreMaster_data['wednesday_open'] == 'Yes'){{ $StoreMaster_data['wednesday_time'] }}@else Closed @endif <br>
                        Thursday - @if($StoreMaster_data['thursday_open'] == 'Yes'){{ $StoreMaster_data['thursday_time'] }}@else Closed @endif <br>
                        Friday - @if($StoreMaster_data['friday_open'] == 'Yes'){{ $StoreMaster_data['friday_time'] }}@else Closed @endif <br>
                        Saturday - @if($StoreMaster_data['saturday_open'] == 'Yes'){{ $StoreMaster_data['saturday_time'] }}@else Closed @endif <br>
                        Sunday - @if($StoreMaster_data['sunday_open'] == 'Yes'){{ $StoreMaster_data['sunday_time'] }}@else Closed @endif
                    </i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['categories']))
                <span class="storedata">
                    <b>CATEGORY:</b>
                    <i>{{ $StoreMaster_data['categories'] }}</i>
                </span>
                @endif
                <a href="javascript:window.history.back();" class="moredetails">
                    « back
                </a>
                <span class="storedata">
                    <a href="#" class="moredetails">
                        <b>VISIT THIS STORE<STORE></STORE></b></a>
                    @if(!empty($StoreMaster_data['whatsapp_number']) && WHATSAPP_DISPLAY != 'false')
                        <a href="{{ REDIRECT_WHATSAPP }}{{ $StoreMaster_data['whatsapp_number'] }}" class="moredetails"><b>VIRTUAL SHOPPING > </b></a>
                    @endif
                </span>
            </div>
            @if(!empty($StoreMaster_data['google_emmbeded_code']))
            <div align="center">
                <div class="halfcontainer google_map">
                    <iframe id="map" src="{{ $StoreMaster_data['google_emmbeded_code']  }}"
                        frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            @endif

            @if(!empty($StoreMaster_data['youtube_link']))
            <div align="center">
                <div class="halfcontainer google_map">
                    <iframe id="map" src="{{ $StoreMaster_data['youtube_link']  }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            @endif
        </div>
    </div>
    <br><br>
    @if(!empty($other_city_html))
        <div id="storelocatorpage">
            {!! $other_city_html !!}
        </div>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        $('.storegallery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 7000,
        });
    </script>
</body>
</html>