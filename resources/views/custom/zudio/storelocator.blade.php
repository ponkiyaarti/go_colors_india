<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state_name) && empty($city_name))
    <title>Westside Stores in {{ ucfirst($state_name) }} - Westside India</title>
    <meta name="description" content="Use our store finder to locate Westside stores in {{ $state_name }}. Get all the list of Westside stores in {{ $state_name }}">
    @elseif(!empty($city_name) && !empty($state_name))
    <title>Westside in {{ ucfirst($city_name) }} | Clothing Stores in {{ ucfirst($city_name) }} - Westside</title>
    <meta name="description" content="Westside store in {{ $city_name }}: Find nearby clothing stores in {{ $city_name }}. Get all the store details of Westside {{ $city_name }}. Shop Now!">
    @else
    <title>Find Clothing Stores Nearby | Westside Store Near You - Westside</title>
    <meta name="description" content="Find the best clothing stores nearby you. Explore the wide range of collections for mens, womens & kids in Westside store near you. Shop from more than 150+ stores in India.">
    @endif

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css">
    <link href="//cdn.shopify.com/s/files/1/0249/4155/9894/t/19/assets/theme.scss.css?v=10152740876973534413" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0249/4155/9894/t/19/assets/custom-styles.scss.css?v=3718649087817362300" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/zudio-storelocatore.css">
</head>

<body>
    <div class="container zudio_file" id="storelocatorpage">
        <div class="loader">
            <div class="preloader">Loading...</div>
        </div>
        <div class="breadcrumb">
            <a href="/" data-translate="general.breadcrumbs.home">Home</a>
            »
            <a href="{{ $get_store_cutome }}">Store Locator</a>
            »
            <a href="{{ $get_store_cutome }}/{{ $state_name }}">{{ ucfirst($state_name) }}</a>
            @if($city_name)
            »
            <a href="{{ $get_store_cutome }}/{{ $state_name }}/{{ $city_name }}">{{ ucfirst($city_name) }}</a>
            @endif
        </div>
        <header class="page-header">
            <br>
            <h2><span>Store Locator</span></h2>
        </header>

        <div class="findthestore">
            <h3 class="head-line">FIND THE {{ strtoupper($type) }} STORE CLOSEST TO YOU</h3>
            <div class="text-content citystoredetails">
                <div class="city select2dropdownwidth">
                    {!!Form::select('store_code',$store_state,$selected_state_name,
                    array('class' => 'form-control select2 country', 'id' => 'store_state'))!!}
                </div>
                @if(!empty($city_name))
                <div class="loc select2dropdownwidth">
                    {!!Form::select('store_city',$city_list,$selected_city_name,
                    array('class' => 'form-control select2', 'id' => 'store_city'))!!}
                </div>
                @else
                <div class="loc select2dropdownwidth">
                    <select name="store_city" class="form-control select2 " id="store_city">
                        <option value>Select City</option>
                    </select>
                </div>
                @endif
            </div>
        </div>
        <div id="render_append">

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" defer></script>
    <script type="text/javascript">
            
        $(document).ready(function () {
            console.log('hi');
            $(".loader").hide();
            // $('.select2').select2();
            var state_data = $('#store_state').val();
            console.log(state_data,'gautam');
            // if(state_data != ""){
            //     getStoreName(state_data);
            // }
            var event_name="{{ $city_name }}";
            // window.store_name="{{ $state_name }}";
            
            if(event_name!='')
            {
                $("#render_append").html('');
                getStoredata(event_name);
            }else{
                if(state_data != ""){
                    getStoreName(state_data);
                }
            }
            if(state_data=='' && event_name=='')
            {
               getDefaultdata();
            }
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getDefaultdata(page);
        });

        $(document).on('change', '#store_state', function (event) {
            if ($(this).val() == '') {
                getDefaultdata();
            }
            else {
                var store_s=$(this).val();
                url_slug="{{ $get_store_cutome }}/"+store_s.toLowerCase().replace(' ','');
                console.log(url_slug);
                window.location.href=url_slug;
            }
        });
        $(document).on('change', '#store_city', function (event) {

            // var var_request= '{{ $city_name }}';

            var store_s=$("#store_state").val();
            
            var store_c=$(this).val();
           
            var store_c=store_c.toLowerCase().replace(' ','');
            url_slug_c="{{ $get_store_cutome }}/"+store_s.toLowerCase().replace(' ','')+'/'+store_c;
                
            console.log(url_slug_c,'citypatel');
            
              // if(store_c!='')
              // {

              //       if ($(this).val() == '') {
              //           if ($('#store_state').val() != '') {
              //               getStoreName($('#store_state').val());
              //           }
              //       }
              //       getStoredata($(this).val());
              //       $("#render_append").html('');
              // }
              // else
              // {
                 window.location.href=url_slug_c;
              // }
        });

        /*shipper */
        function getStoreName(val) {
            $.ajax({
                url: '{{ $get_store_name_url }}',
                type: 'post',
                data: { "store_state": val },
                dataType: 'json',
                beforeSend: function () {
                    $(".loader").show();
                },
                success: function (states) {
                    console.log('states');
                    console.log(states);
                    $(".loader").hide();
                    $.each(states.data, function (v, k) {
                        $('#store_city').append('<option value="' + k + '">' + v + '</option>')
                    });
                    // $('#select2-store_city-container').text('Select City');
                    $("#render_append").html(states.render);
                }
            });
        }
        function getStoredata(val) {
            
            $.ajax({
                url: '{{ $get_store_data_url }}',
                type: 'post',
                data: { "store_city": val },
                dataType: 'json',
                beforeSend: function () {
                    $(".loader").show();
                },
                success: function (states) {
                    
                    $(".loader").hide();
                    $("#render_append").html(states.data);
                }
            });
        }
        function getDefaultdata(page = null) {
            url = "{{ $get_store_all_url }}";
            if (page != null) {
                url += "&page=" + page;
            }
            console.log('pagination',url);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (states) {
                    $("#render_append").html(states.data);
                }
            });
        }
    </script>
</body>

</html>