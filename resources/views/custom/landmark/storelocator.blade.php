<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state_name) && empty($city_name))
    <title>Westside Stores in {{ ucfirst($state_name) }} - Westside India</title>
    <meta name="description" content="Use our store finder to locate Westside stores in {{ $state_name }}. Get all the list of Westside stores in {{ $state_name }}">
    @elseif(!empty($city_name) && !empty($state_name))
    <title>Westside in {{ ucfirst($city_name) }} | Clothing Stores in {{ ucfirst($city_name) }} - Westside</title>
    <meta name="description" content="Westside store in {{ $city_name }}: Find nearby clothing stores in {{ $city_name }}. Get all the store details of Westside {{ $city_name }}. Shop Now!">
    @else
    <title>Find Clothing Stores Nearby | Westside Store Near You - Westside</title>
    <meta name="description" content="Find the best clothing stores nearby you. Explore the wide range of collections for mens, womens & kids in Westside store near you. Shop from more than 150+ stores in India.">
    @endif

    
    <link href="//cdn.shopify.com/s/files/1/0477/5025/0652/t/21/assets/styles.css?v=17861021307432912759" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0477/5025/0652/t/21/assets/static-page.scss?v=8517909837194328315" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/landmark-storelocatore.css">
    <style>
        .heading{
            display: flex;
            justify-content: flex-start;
        }
    </style>
</head>

<body>
    <div class="container zudio_file" id="storelocatorpage">
        <div class="loader">
            <div class="preloader">Loading...</div>
        </div>
        <div class="breadcrumb">
            <a href="/" data-translate="general.breadcrumbs.home">Home</a>
            »
            <a href="{{ $get_store_cutome }}">Store Locator</a>
        </div>
        <header class="page-header heading">
            <br>
            <h2><span>Store Locator</span></h2>
        </header>

        <div id="render_append">

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" defer></script>
    <script type="text/javascript">
            
        $(document).ready(function () {

            @if(!empty($state_name) && empty($city_name))
                document.title = "Landmark Stores in {{ ucfirst($state_name) }} - Landmark Xcite";
                $('meta[name=description]').attr("content", "Landmark Xcite is an online shopping store in {{ $state_name }}  for books, toys, music, fashion, tech, sporting goods & more in {{ $state_name }}. Shop from Landmark stores in {{ $state_name }}");

            @elseif(!empty($city_name) && !empty($state_name))
                document.title = "Landmark Stores in {{ ucfirst($city_name) }} - Landmark Xcite";
                $('meta[name=description]').attr("content", "Landmark Xcite is an online shopping store in {{ $city_name }} for books, toys, music, fashion, tech, sporting goods & more in {{ $city_name }}. Shop from Landmark stores in {{ $city_name }}");
            @else
                document.title = "Landmark Stores in India | Landmark Online Shopping in India";
                $('meta[name=description]').attr("content", "Landmark Xcite is an online shopping store in India for books, toys, music, fashion, tech, sporting goods & more in India. Shop from Landmark stores in India");
            @endif

            console.log('hi');
            $(".loader").hide();
            // $('.select2').select2();
            let searchParams = window.location.pathname;
            pathArray = searchParams.split( '/' );
            var event_name='';
            var state_data='';
            if(pathArray[4]!=undefined)
            {
                var state_data=pathArray[4];
            }
            if(pathArray[5]!=undefined)
            {
                var event_name=pathArray[5];
            }
            console.log(event_name);

            //var state_data = $('#store_state').val();
            //console.log(state_data,'gautam');
            // if(state_data != ""){
            //     getStoreName(state_data);
            // }
            //var event_name="{{ $city_name }}";
            // window.store_name="{{ $state_name }}";
            
            if(event_name!='')
            {
                $("#render_append").html('');
                getStoredata(event_name);
            }else{
                if(state_data != ""){
                    getStoreName(state_data);
                }
            }
            if(state_data=='' && event_name=='')
            {
               getDefaultdata();
            }
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getDefaultdata(page);
        });

        $(document).on('change', '#store_state', function (event) {
            if ($(this).val() == '') {
                getDefaultdata();
            }
            else {
                var store_s=$(this).val();
                url_slug="{{ $get_store_cutome }}/"+store_s.toLowerCase().replace(' ','');
                console.log(url_slug);
                window.location.href=url_slug;
            }
        });
        $(document).on('change', '#store_city', function (event) {

            // var var_request= '{{ $city_name }}';

            var store_s=$("#store_state").val();
            
            var store_c=$(this).val();
           
            var store_c=store_c.toLowerCase().replace(' ','');
            url_slug_c="{{ $get_store_cutome }}/"+store_s.toLowerCase().replace(' ','')+'/'+store_c;
                
            console.log(url_slug_c,'citypatel');
            
              // if(store_c!='')
              // {

              //       if ($(this).val() == '') {
              //           if ($('#store_state').val() != '') {
              //               getStoreName($('#store_state').val());
              //           }
              //       }
              //       getStoredata($(this).val());
              //       $("#render_append").html('');
              // }
              // else
              // {
                 window.location.href=url_slug_c;
              // }
        });

        /*shipper */
        function getStoreName(val) {
            $.ajax({
                url: '{{ $get_store_name_url }}',
                type: 'post',
                data: { "store_state": val },
                dataType: 'json',
                beforeSend: function () {
                    $(".loader").show();
                },
                success: function (states) {
                    console.log('states');
                    console.log(states);
                    $(".loader").hide();
                    $.each(states.data, function (v, k) {
                        $('#store_city').append('<option value="' + k + '">' + v + '</option>')
                    });
                    // $('#select2-store_city-container').text('Select City');
                    $("#render_append").html(states.render);
                }
            });
        }
        function getStoredata(val) {
            
            $.ajax({
                url: '{{ $get_store_data_url }}',
                type: 'post',
                data: { "store_city": val },
                dataType: 'json',
                beforeSend: function () {
                    $(".loader").show();
                },
                success: function (states) {
                    
                    $(".loader").hide();
                    $("#render_append").html(states.data);
                }
            });
        }
        function getDefaultdata(page = null) {
            url = "{{ $get_store_all_url }}";
            if (page != null) {
                url += "&page=" + page;
            }
            console.log('pagination',url);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (states) {
                    $("#render_append").html(states.data);
                }
            });
        }
    </script>
</body>

</html>