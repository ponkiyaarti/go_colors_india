@if(!empty($Store_data))
    <header class="page-header common centeralign">
        <h1><span>@if($store_count!=0) @if(!empty($StoreName)) {{ $StoreName }} @else ALL @endif stores detail @php if($store_count!=0) echo "($store_count)"; @endphp @endif</span></h1>
    </header>
    <div class="storesdetail">
        @foreach($Store_data as $key => $value)
        <div class="storedetails">
            <h3>{{ $value['store_name'] }}</h3>
            <h5>Address</h5>
            <p>
                {{ $value['store_address'] }}
                @if(!empty($value['store_city']))
                , {{ $value['store_city'] }}
                @endif
                @if(!empty($value['store_state']))
                , {{ $value['store_state'] }}
                @endif
                @if(!empty($value['store_pin_code']))
                , {{ $value['store_pin_code'] }}
                @endif
            </p>
            <!-- <h5>Phone No:</h5>
            <p>{{ $value['store_mobile_number'] }}</p> -->
            <a href="{{ $detail_url }}/{{ strtolower(str_replace(' ', '', $value['store_state'])) }}/{{ strtolower(str_replace(' ', '', $value['store_city'])) }}/{{ $value['slug'] }}?type=landmark" class="moredetails"><b>MORE DETAILS > </b></a>   
            @if(!empty($value['whatsapp_number']) && WHATSAPP_DISPLAY != 'false')
            <a href="{{ REDIRECT_WHATSAPP }}{{ $value['whatsapp_number'] }}" class="moredetails"><b>VIRTUAL SHOPPING > </b></a>
            @endif
        </div>
        @endforeach
    </div>
    @if($Store_data instanceof \Illuminate\Pagination\LengthAwarePaginator)
    <div class="paginationlinks">
        {{ $Store_data->links() }}
    </div>
    @endif
@endif