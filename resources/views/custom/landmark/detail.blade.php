<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state) && !empty($city) && !empty($slug))
    <title>Westside Store in {{ $slug }} - Westside</title>
    <meta name="description" content="Use our store finder to locate Westside stores in {{ $slug }}. Know more about the Westside {{ $slug }} store timings, address, directions & contact number.">
    @else
    <title>storelocator</title>
    @endif

     <link href="//cdn.shopify.com/s/files/1/0477/5025/0652/t/21/assets/styles.css?v=17861021307432912759" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0477/5025/0652/t/21/assets/static-page.scss?v=8517909837194328315" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/landmark-storelocatore.css">
</head>
<body>
    <div class="container" id="storelocatorpage">
        <div class="breadcrumb">
            <a href="/" data-translate="general.breadcrumbs.home">Home</a>
            »
            <a href="{{ $detail_url }}s">Store Locator</a>
            »
           @if(\Request::segment(2)=="landmark")
           <a href="{{ $detail_url }}s/{{ \Request::segment(4) }}?type={{$type}}">{{ \Request::segment(4) }}</a>
            »
           <a href="{{ $detail_url }}s/{{ \Request::segment(4) }}/{{ \Request::segment(5) }}?type={{$type}}">{{ \Request::segment(5) }}</a>
           @else
              <a href="{{ $detail_url }}s/{{ \Request::segment(3) }}?type={{$type}}">{{ \Request::segment(3) }}</a>
            »
           <a href="{{ $detail_url }}s/{{ \Request::segment(3) }}/{{ \Request::segment(4) }}?type={{$type}}">{{ \Request::segment(4) }}</a>
           @endif
            <!-- <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <span>{{ \Request::segment(6) }}</span> -->
        </div>
        @if(!empty($desktop_store_gallery))
        <div class="storegallery desktop_image">
            @foreach($desktop_store_gallery as $value)
            <div>
                <img src="{{ IMAGES_PATH }}{{ $value['images'] }}">
            </div>
            @endforeach
        </div>
        @endif
        @if(!empty($mobile_store_gallery))
        <div class="storegallery mobile_image">
            @foreach($mobile_store_gallery as $value)
            <div>
                <img src="{{ IMAGES_PATH }}{{ $value['images'] }}">
            </div>
            @endforeach
        </div>
        @endif
        <div class="storecontentgallery">
            <div class="storecontent">
                <h1>{{ $StoreMaster_data['store_name'] }}</h1>
                @if(!empty($StoreMaster_data['store_address']))
                <span class="storedata">
                    <b>ADDRESS:</b>
                    <i>
                        {{ $StoreMaster_data['store_address'] }}
                        @if(!empty($StoreMaster_data['store_city']))
                        , {{ $StoreMaster_data['store_city'] }}
                        @endif
                        @if(!empty($StoreMaster_data['store_state']))
                        , {{ $StoreMaster_data['store_state'] }}
                        @endif
                        @if(!empty($StoreMaster_data['store_pin_code']))
                        , {{ $StoreMaster_data['store_pin_code'] }}
                        @endif
                    </i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['store_mobile_number']))
                <!-- <span class="storedata">
                    <b>PHONE:</b>
                    <i><a
                            href="tel:{{ $StoreMaster_data['store_mobile_number'] }}">{{ $StoreMaster_data['store_mobile_number'] }}</a></i>
                </span> -->
                @endif
                @if(!empty($StoreMaster_data['whatsapp_number']))
                <span class="storedata">
                    <b>Whatsapp Number:</b>
                    <i><a
                            href="tel:{{ $StoreMaster_data['whatsapp_number'] }}">{{ $StoreMaster_data['whatsapp_number'] }}</a></i>
                </span>
                @endif
                @if(!empty($StoreMaster_data['monday_time']) || !empty($StoreMaster_data['tuesday_time']) ||
                !empty($StoreMaster_data['wednesday_time']) || !empty($StoreMaster_data['thursday_time']) ||
                !empty($StoreMaster_data['friday_time']) || !empty($StoreMaster_data['saturday_time']) ||
                !empty($StoreMaster_data['sunday_time']))
                {{-- <span class="storedata">
                    <b>OPENING HOURS:</b>
                    <i>
                        Monday - @if($StoreMaster_data['monday_open'] == 'Yes'){{ $StoreMaster_data['monday_time'] }}@else Closed @endif<br>
                        Tuesday - @if($StoreMaster_data['tuesday_open'] == 'Yes'){{ $StoreMaster_data['tuesday_time'] }}@else Closed @endif <br>
                        Wednesday - @if($StoreMaster_data['wednesday_open'] == 'Yes'){{ $StoreMaster_data['wednesday_time'] }}@else Closed @endif <br>
                        Thursday - @if($StoreMaster_data['thursday_open'] == 'Yes'){{ $StoreMaster_data['thursday_time'] }}@else Closed @endif <br>
                        Friday - @if($StoreMaster_data['friday_open'] == 'Yes'){{ $StoreMaster_data['friday_time'] }}@else Closed @endif <br>
                        Saturday - @if($StoreMaster_data['saturday_open'] == 'Yes'){{ $StoreMaster_data['saturday_time'] }}@else Closed @endif <br>
                        Sunday - @if($StoreMaster_data['sunday_open'] == 'Yes'){{ $StoreMaster_data['sunday_time'] }}@else Closed @endif
                    </i>
                </span> --}}
                @endif
                @if(!empty($StoreMaster_data['categories']))
                <span class="storedata">
                    <b>CATEGORY:</b>
                    <i>{{ $StoreMaster_data['categories'] }}</i>
                </span>
                @endif
                <a href="javascript:window.history.back();" class="moredetails">
                   <b>« Back</b>
                </a>
                <br><br><br>
                <!-- <span class="storedata">
                    <a href="#" class="moredetails">
                        <b>VISIT THIS STORE<STORE></STORE></b></a>
                    @if(!empty($StoreMaster_data['whatsapp_number']) && WHATSAPP_DISPLAY != 'false')
                        <a href="{{ REDIRECT_WHATSAPP }}{{ $StoreMaster_data['whatsapp_number'] }}" class="moredetails"><b>VIRTUAL SHOPPING > </b></a>
                    @endif
                </span> -->
            </div>
            @if(!empty($StoreMaster_data['google_emmbeded_code']))
            <div align="center">
                <div class="google_map">
                    <iframe id="map" src="{{ $StoreMaster_data['google_emmbeded_code']  }}"
                        frameborder="0" style="border:0;width: 98%" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            @endif
        </div>
    </div>
    <br>
    @if(!empty($other_city_html))
        <div id="storelocatorpage">
            {!! $other_city_html !!}
        </div>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        @if(!empty($slug))
            document.title = "  Landmark Stores in {{ $slug }} - Landmark Xcite";
            $('meta[name=description]').attr("content", "Landmark Xcite is an online shopping store in  for books, toys, music, fashion, tech, sporting goods & more. Shop from Landmark stores in {{ $slug }}");
        @else
            document.title = "storelocator";
        @endif

        $('.storegallery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 7000,
        });
    </script>
</body>
</html>