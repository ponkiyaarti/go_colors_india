<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(!empty($state_name) && empty($city_name))
    <title>Westside Stores in {{ ucfirst($state_name) }} - Westside India</title>
    <meta name="description" content="Use our store finder to locate Westside stores in {{ $state_name }}. Get all the list of Westside stores in {{ $state_name }}">
    @elseif(!empty($city_name) && !empty($state_name))
    <title>Westside in {{ ucfirst($city_name) }} | Clothing Stores in {{ ucfirst($city_name) }} - Westside</title>
    <meta name="description" content="Westside store in {{ $city_name }}: Find nearby clothing stores in {{ $city_name }}. Get all the store details of Westside {{ $city_name }}. Shop Now!">
    @else
    <title>Find Clothing Stores Nearby | Westside Store Near You - Westside</title>
    <meta name="description" content="Find the best clothing stores nearby you. Explore the wide range of collections for mens, womens & kids in Westside store near you. Shop from more than 150+ stores in India.">
    @endif

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css">
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/storelocator.css">
    <link href="//cdn.shopify.com/s/files/1/0266/6276/4597/t/49/assets/vendor.min.css?v=11283820616272568804" rel="stylesheet" type="text/css" media="all" />
    <link href="//cdn.shopify.com/s/files/1/0266/6276/4597/t/49/assets/theme-styles.scss.css?v=4000088586524479405" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://customapp.trent-tata.com/css/storelocator.css">

</head>

<body>
    <div class="container" id="storelocatorpage">
        <div class="loader">
            <div class="preloader">Loading...</div>
        </div>
        <div class="breadcrumb">
            <a href="/" data-translate="general.breadcrumbs.home">Home</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $get_store_cutome }}">Store Locator</a>
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $get_store_cutome }}/{{ $state_name }}">{{ $state_name }}</a>
            @if($city_name)
            <span class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
            <a href="{{ $get_store_cutome }}/{{ $state_name }}/{{ $city_name }}">{{ $city_name }}</a>
            @endif
        </div>
        <header class="page-header">
            <h2><span>Store Locator</span></h2>
        </header>

      
        
    
        <div class="findthestore">
            <h3 class="head-line">FIND THE GO COLORS STORE CLOSEST TO YOU</h3>
            <div class="text-content citystoredetails">
                <div class="city select2dropdownwidth">
                    {!!Form::select('store_code',$store_state,$selected_state_name,
                    array('class' => 'form-control select2 country', 'id' => 'store_state'))!!}
                </div>
                @if(!empty($city_name))
                <div class="loc select2dropdownwidth">
                    {!!Form::select('store_city',$city_list,$selected_city_name,
                    array('class' => 'form-control select2', 'id' => 'store_city'))!!}
                </div>
                @else
                <div class="loc select2dropdownwidth">
                    <select name="store_city" class="form-control select2" id="store_city">
                        <option value>Select City</option>
                    </select>
                </div>
                @endif
            </div>
        </div>
        <div id="render_append">

        </div>
    </div>

 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/select2.min.js" defer></script>

    <script type="text/javascript">
            
        $(document).ready(function () {

            @if(!empty($state_name) && empty($city_name))
                document.title = "GO COLORS Stores in {{ ucfirst($state_name) }} - GO COLORS India";
                $('meta[name=description]').attr("content", "Use our store finder to locate GO COLORS stores in {{ $state_name }}. Get all the list of GO COLORS stores in {{ $state_name }}");

            @elseif(!empty($city_name) && !empty($state_name))
                document.title = "GO COLORS in {{ ucfirst($city_name) }} | Clothing Stores in {{ ucfirst($city_name) }} - GO COLORS";
                $('meta[name=description]').attr("content", "GO COLORS store in {{ $city_name }}: Find nearby clothing stores in {{ $city_name }}. Get all the store details of GO COLORS {{ $city_name }}. Shop Now!");
            @else
                document.title = "Find Clothing Stores Nearby | GO COLORS Store Near You - GO COLORS";
                $('meta[name=description]').attr("content", "Find the best clothing stores nearby you. Explore the wide range of collections for mens, womens & kids in GO COLORS store near you. Shop from more than 150+ stores in India.");
            @endif

            $(".loader").hide();
            $('.select2').select2();
            var state_data = $('#store_state').val();
            console.log(state_data,'gautam');
            
            var event_name="{{ $city_name }}";


            if(event_name!='')
            {
                $("#render_append").html('');
                getStoredata(event_name);
            }else{
                if(state_data != ""){
                    getStoreName(state_data);
                }
            }
            if(state_data=='' && event_name=='')
            {
               getDefaultdata();
            }
        });

        $(document).on('click', '.pagination a', function (event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            getDefaultdata(page);
        });

        $("#store_state").on('change', function () {
            if ($(this).val() == '') {
                getDefaultdata();
            }
            else {
                var store_s=$(this).val();
                url_slug="{{ $get_store_cutome }}"+store_s.toLowerCase().replace(' ','');
                console.log(url_slug);
                window.location.href=url_slug;
            }
        });
        $("#store_city").on('change', function () {

            var store_s=$("#store_state").val();
            
            var store_c=$(this).val();
           
            var store_c=store_c.toLowerCase().replace(' ','');
            url_slug_c="{{ $get_store_cutome }}"+store_s.toLowerCase().replace(' ','')+'/'+store_c;
                
            console.log(url_slug_c,'citypatel');
                window.location.href=url_slug_c;
        });

        /*shipper */
        function getStoreName(val) {
            $.ajax({
                url: '{{ $get_store_name_url }}',
                type: 'post',
                data: {  "_token": "{{ csrf_token() }}","store_state": val },
                dataType: 'json',
                beforeSend: function () {
                    //$(".loader").show();
                },
                success: function (states) {
                    console.log('states');
                    console.log(states);
                    $(".loader").hide();
                    $.each(states.data, function (v, k) {
                        $('#store_city').append('<option value="' + k + '">' + v + '</option>')
                    });
                    // $('#select2-store_city-container').text('Select City');
                    $("#render_append").html(states.render);
                }
            });
        }
        function getStoredata(val) {
            $.ajax({
                url: '{{ $get_store_data_url }}',
                type: 'post',
                data: {  "_token": "{{ csrf_token() }}","store_city": val },
                dataType: 'json',
                beforeSend: function () {
                    //$(".loader").show();
                },
                success: function (result) {
                    console.log("result",result.data);
            
                    $(".loader").hide();
                    $("#render_append").html(result.data);
                }
            });
        }
        function getDefaultdata(page = null) {
            // alert(page);
            url = "{{ $get_store_all_url }}";
            if (page != null) {
                url += "?page=" + page;
            }

            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                },
                success: function (states) {
                    $("#render_append").html(states.data);
                }
            });
        }
    </script>
</body>

</html>