
<!DOCTYPE html>
<html>
<head>
    <title>Westside</title>
    <?= Html::style('backend/css/sweetalert.min.css',[],IS_SECURE) ?>
    <?= Html::style('backend/css/toster.min.css',[],IS_SECURE) ?>
    <style type="text/css">
        #return_table input {
        display: inline-block;
    }
    </style>
</head>
<body>

<div id="order_data"></div>



</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">

    var token = "<?=csrf_token()?>";
    $(window).on('load',function(){

        var q = document.URL.split('&')[1];

        if(q != undefined) {


          var hash = q.split('=');
          var webview = hash[1];
          console.log(webview);
          if(webview == "yes"){
            var links = document.links;

            $.each(links , function(key,value){
              value.setAttribute('href', value.href+'?webview=yes');

            });
            var showHeader = "false";
          }else{
            var showHeader = "true";
          }
        }
        if(showHeader == "true")
        {
          $('#shopify-section-sidebar-section').show();
          $('.wrapper-header').show();
          $('.footer_custom_container').show();
          $('.breadcrumb').show();
        }
      });

        
        // var val = $(this).val();
        var order_id = "{{$order_name}}";
        var url = "{{$get_order_detail_url}}";
        var method_type = 'get';
        console.log(url);
        console.log(method_type);
        console.log(order_id);
        $.ajax({

            url:url,
            type:method_type,
            data:{"order_no":order_id},
            dataType: 'json',
            beforeSend:function(){

                $("[id$='_error']").empty();

                $(".loader").addClass('fa fa-spinner fa-spin');

                $("#save").attr('disabled','disabled');  
            },
            success : function(resp)

            {  
                if(resp.success == true)

                {
                    $('#order_data').html('');
                    $('#order_data').append(resp.data);

                }
                $(".loader").removeClass('fa fa-spinner fa-spin');

                $("#save").removeAttr('disabled');    

            },
        })

        /*$('#main_form').ajaxSubmit({

            url: url,

            type: method_type,

            data: { "_token" : token },

            dataType: 'json',

            beforeSubmit : function()

            {
                  

            },

            success : function(resp)

            {  
                if(resp.success == true)

                {
                    $('#order_data').html('');
                    $('#order_data').append(resp.data);

                }
                $(".loader").removeClass('fa fa-spinner fa-spin');

                $("#save").removeAttr('disabled');    

            },

            error : function(respObj){

                $(".loader").removeClass('fa fa-spinner fa-spin');

                $("#save").removeAttr('disabled');    

                // toastr.error('Something went wrong');    

                $.each(respObj.responseJSON.errors, function(k,v){

                    $('#'+k+'_error').text(v);

                });

                $(".overlay").hide();

            }

        });*/

    // });
    /*$(document).on('click','.return_order_submit',function(e){
       e.preventDefault();
       
        var btn_name = $(this).attr('title');
        var val = $(this).val();
        var url = "{{$return_order_url}}";
        var method_type = 'post';
        var token = "<?=csrf_token()?>";

        var selected_checkbox = $('#return_table tbody input[type=checkbox]:checked');
        
        if(selected_checkbox.size() == 0)
        {
            
            swal({
               title: "Please Select Item For Refund",
               type:"error",
               timer: 2000,
               showConfirmButton: false 
            });
        }
        else
        {   
            // console.log(selected_checkbox.size());   
            // console.log(selected_checkbox.attr('value'));
            var form = $('form.return_form').serialize();
            // form._token = token;
            $.ajax({
                url : url,
                type:method_type,
                dataType:'json',
                data:form,
                beforeSend:function(){

                    $("[id$='_error']").empty();

                    $(".loader").addClass('fa fa-spinner fa-spin');

                    $("#save").attr('disabled','disabled');  
                },
                success : function(resp)
        
                {  
                    console.log('success');
                    console.log(resp);
                    if(resp.success == true)
        
                    {
                        toastr.success('Order Return Successfully');
                        window.location.reload();
        
                    }
                    else{
                        toastr.error('Something Went Wrong');
                    }

                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                },
                error : function(respObj){
                    
                    console.log('err');
                    console.log(respObj);
                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                    toastr.error('Something went wrong');
                    $.each(respObj.responseJSON.errors, function(k,v){
        
                        $('#'+k+'_error').text(v);
        
                    });
        
                    $(".overlay").hide();
        
                }

            })*/
            /*$('form.return_form').ajaxSubmit({
    
                url: url,
        
                type: method_type,
        
                data: { "_token" : token},
        
                dataType: 'json',
        
                beforeSubmit : function()
        
                {
                    $("[id$='_error']").empty();
        
                    $(".loader").addClass('fa fa-spinner fa-spin');
        
                      $(".save").attr('disabled','disabled');    
        
                },
        
                success : function(resp)
        
                {  
                    if(resp.success == true)
        
                    {
                        toastr.success('Order Return Successfully');
                        window.location.reload();
        
                    }
                    else{
                        toastr.error('Something Went Wrong');
                    }

                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                },
                error : function(respObj){
        
                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                    toastr.error('Something went wrong');
                    $.each(respObj.responseJSON.errors, function(k,v){
        
                        $('#'+k+'_error').text(v);
        
                    });
        
                    $(".overlay").hide();
        
                }
            });*/
            

        // }

        // var data = $('form.return_form').serializeArray();
        // console.log(data);

        
    // });
    $(document).on('click','.return_order_submit',function(e){
        e.preventDefault();
	
        var btn_name = $(this).attr('title');
        var val = $(this).val();
        var url = "{{$return_order_url}}";
        var method_type = 'post';
	   var token = "<?=csrf_token()?>";
	   

        var selected_checkbox = $('#return_table tbody input[type=checkbox]:checked');
        
        if(selected_checkbox.size() == 0)
        {
            console.log('hello');
            swal({
               title: "Please Select Item For Return",
               type:"error",
               timer: 2000,
               showConfirmButton: false 
            });
        }else{
    		$('form.return_form').ajaxSubmit({
    
                url: url,
        
                type: method_type,
        
                data: { "_token" : token},
        
                dataType: 'json',
        
                beforeSubmit : function()
        
                {
                    $("[id$='_error']").empty();
        
                    $(".loader").addClass('fa fa-spinner fa-spin');
        
                      $(".save").attr('disabled','disabled');    
        
                },
        
                success : function(resp)
        
                {  
                    if(resp.success == true)
        
                    {
                        toastr.success('Order Return Successfully');
                        window.location = '/account';
                        // window.location.reload();
        
                    }
                    else{
                        toastr.error('Something Went Wrong');
                        $('.error_msg').text(resp.message);
                        // window.location = '/account';
                    }

                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                },
                error : function(respObj){
                    // console.log(respObj);
                    $(".loader").removeClass('fa fa-spinner fa-spin');
        
                    $(".save").removeAttr('disabled');    
        
                    toastr.error('Something went wrong');
                    $('.error_msg').text(respObj.message);
                    // $.each(respObj.responseJSON.errors, function(k,v){
        
                    //     $('#'+k+'_error').text(v);
        
                    // });
        
                    $(".overlay").hide();
        
                }
            });
	}

    });             
</script>
