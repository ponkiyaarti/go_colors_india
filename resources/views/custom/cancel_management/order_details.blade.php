<!DOCTYPE html>
<html>
<head>
    <title>Westside</title>
    <?= Html::style('backend/css/sweetalert.min.css',[],IS_SECURE) ?>
    <style type="text/css">
        #cancel_table input {
        display: inline-block;
    }
    </style>
</head>
<body>
    <div id="order_data"></div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script type="text/javascript">
    var token = "<?=csrf_token()?>";
    $(document).ready(function(e){
        var order_id = "{{$order_name}}";
        var url = "{{$get_order_detail_url}}";
        var method_type = 'get';
        $.ajax({
            url:url,
            type:method_type,
            data:{"order_no":order_id},
            dataType: 'json',
            beforeSend:function(){
                $("[id$='_error']").empty();
                $(".loader").addClass('fa fa-spinner fa-spin');
                $("#save").attr('disabled','disabled');  
            },
            success : function(resp)
            {  
                if(resp.success == true)
                {
                    $('#order_data').html('');
                    $('#order_data').append(resp.data);
                }
                $(".loader").removeClass('fa fa-spinner fa-spin');
                $("#save").removeAttr('disabled');    
            },
        })
    });

    $(document).on('click','.cancel_order_submit',function(e){
       e.preventDefault();

        var btn_name = $(this).attr('title');
        var val = $(this).val();
        var url = "{{$return_order_url}}";
        var method_type = 'post';
        var token = "<?=csrf_token()?>";
        var selected_checkbox = $('#cancel_table tbody input[type=checkbox]:checked');
        if(selected_checkbox.size() == 0)
        {
            swal({
               title: "Please Select Item For Cancel",
               type:"error",
               timer: 2000,
               showConfirmButton: false 
            });
        }
        else
        {
            var form = $('form.cancel_form').serialize();
            $.ajax({
                url : url,
                type:method_type,
                dataType:'json',
                data:form,
                beforeSend:function(){
                    $("[id$='_error']").empty();
                    $(".loader").addClass('fa fa-spinner fa-spin');
                    $("#save").attr('disabled','disabled');  
                },
                success : function(resp){
                    if(resp.success == true)
                    {
                        window.location.href = '/account';
                    }else{
                        $('#cancel_qty_error').text(resp.message);
                    }
                    $(".loader").removeClass('fa fa-spinner fa-spin');
                    $(".save").removeAttr('disabled');    
                },
                error : function(respObj){
                    $(".loader").removeClass('fa fa-spinner fa-spin');
                    $(".save").removeAttr('disabled');
                    $('#cancel_qty_error').text(respObj.message);
                    $(".overlay").hide();
                }
            })
        }
    });
</script>
