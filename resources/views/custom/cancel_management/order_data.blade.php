<style type="text/css">
#cancel_table input {
    display: inline-block;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                @if(isset($order_data['customer']))
                <div class="col-md-6">
                    <section class="hk-sec-wrapper">
                        <h5>Customer Information</h5><hr>  
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Name</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['first_name'].' '.$order_data['customer']['last_name'] ?><label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['email'] ?><label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Order Date</label>
                            <div class="col-sm-8">
                              <label class="col-form-label"><?= $order_data['customer']['created_at'] ?><label>
                            </div>
                        </div>
                        
                    </section>
                </div>
                @endif
                @if(!empty($shipping_address))
                <div class="col-md-6">
                    <section class="hk-sec-wrapper">
                        <h5>Shipping Address Information</h5><hr>  
                        
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Address</label>
                            <div class="col-sm-8">
                              <label class="col-form-label">
                                @if(!empty($shipping_address['address1']))
                                <?=$shipping_address['address1'] ?>,
                                @endif
                                
                                @if(!empty($shipping_address['address2']))
                                <?=$shipping_address['address2'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['city']))
                                <?=$shipping_address['city'] ?>,
                                @endif

                                @if(!empty($shipping_address['province']))
                                <?=$shipping_address['province'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['country']))
                                <?=$shipping_address['country'] ?>,
                                @endif
                                @if(!empty($shipping_address['zip']))
                                <?=$shipping_address['zip'] ?>,
                                @endif
                                <br>
                                @if(!empty($shipping_address['phone']))
                                <?=$shipping_address['phone'] ?>,
                                @endif

                              </label>
                            </div>
                        </div>
                        
                    </section>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

{{ Form::open(['class'=>'cancel_form']) }}
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <section class="hk-sec-wrapper">
                <h5>Order Item</h5><hr>  
                <table class="table table-bordered cancel_table" id="cancel_table" style="width: 100%">
                    <thead>
                          <tr>
                            <th></th>
                            <th>ITEM</th>
                            <th>SKU</th>
                            <th>QTY</th>
                            <th>CANCEL QTY</th>
                        </tr>
                    </thead>
                    <tbody>
                        <input type="hidden" name="shopify_order_id" value="{{ $order_data['id'] }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        @foreach($line_items as $line_item_key => $line_item_value)
                        <tr>
                            <td class="animated-checkbox">
                                <input type="checkbox" id="chk_{{ $line_item_value['id'] }}" name="line_item[]" value="{{ $line_item_value['id'] }}"/>
                            </td>
                            <td><?= $line_item_value['title'] ?></td>
                            <td><?= $line_item_value['sku'] ?></td>
                            <td><?= $line_item_value['quantity'] ?></td>
                            <td width="30%">
                                <input type="hidden" name="{{ $line_item_value['id'] }}">                            
                                <input type="hidden" name="item_price[{{ $line_item_value['id'] }}]" value="{{ $line_item_value['price'] }}" size="2">

                                <select name="cancel_qty[{{ $line_item_value['id'] }}]" class="quantity">
                                    <option value="0">Select Cancel quantity</option>
                                    @for($i=1;$i<=$line_item_value['quantity'];$i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </td>
                         </tr> 
                        @endforeach
                    </tbody>
                </table>
                <span id="cancel_qty_error" class="help-inline text-danger error_msg"></span>
                <div class="text-align-right canceldiv">
                    <button type="submit" class="btn btn-primary btn-sm mr-2 buttonOverAnimation cancel_order_submit"  title="Cancel Order"><i class="loader"></i>Cancel Order</button>
                </div>
            </section>
        </div>
    </div>
</div>
{{ Form::close() }}