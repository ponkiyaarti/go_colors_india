<style>
    .main_div {
        height: 100vh;
        width: 100%;
    }

    .flex_class {
        display: flex;
        justify-content: center;
        align-items: center;
       flex-direction: column;
    }

    .main_div .form-group {
        width: 90%;
        margin: 0 auto;
    }

    .main_div .form-group>* {
        margin-bottom: 15px;
        margin-left: auto;
        margin-right: auto;
        height: 40px;
        width: 35%;
        border-radius: 5px;
        border: none;
    }

    .main_div .form-group input {
        border: 1px solid #000;
        padding: 0 15px;
    }

    .main_div .form-group button {
        cursor: pointer;
        width: 20%;
        margin: 0;
        margin-right: 10px;
    }
    .button_group{
        flex-direction: row;
    }
</style>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Go Colors India</title>
</head>

<body>
    <div class="main_div flex_class">
        <div class="form-group flex_class">
            <input type="tel" name="mobile" id="mobile" placeholder="Enter Mobile Number">
            <input type="tel" name="otp" id="otp" placeholder="Enter OTP" required="" maxlength="6">
        </div>
        <div class="form-group flex_class button_group">
            <button type="submit" id="send_otp" class="send_otp">Send Otp</button>
            <button type="submit" id="verify_otp">Verify Otp</button>
            <button type="submit" id="resend_otp" class="send_otp">Resend Otp</button>
        </div>
    <span id="err_msg"></span>
    </div>
</body>

</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#otp").hide();
    $("#verify_otp").hide();
    $("#resend_otp").hide();
});

$("#mobile").on('keypress', function (event) {
    var mobile = $('#mobile').val();
    if(mobile.length>=10)
    {
        return false;
    }
    return (((event.which > 47) && (event.which < 58)) || (event.which == 13));
});
$(".send_otp").click(function(e){

    var mobile = $('#mobile').val();
    var url = "{{URL::route('send.otp')}}";
    $.ajax({
        url: url,
        type: "POST",
        data:{"mobile":mobile},
        success: function(data){
            $("#err_msg").text(data.message);
            if(data.success == true)
            {
              $("#otp").show();
              $("#verify_otp").show();
              $("#resend_otp").show();
              $("#send_otp").hide();
            }
        }
    });
});

$("#verify_otp").click(function(e){
    
    var mobile = $('#mobile').val();
    var otp = $('#otp').val();
    var url = "{{URL::route('verify.otp')}}";
    $.ajax({
        url: url,
        type: "POST",
        data:{"mobile":mobile, 'otp':otp},
        success: function(data){
            console.log(data)
            $("#err_msg").text(data.message);
            if(data.success == true)
            {
                $("#verify_otp").hide();     
               // alert("login success");
            }else{
                //alert("login fail");
            }
        }
    });
});

$("#resend_otp").click(function(e){
     $("#otp").val("");
});
</script>