<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CustomerLoginController;
use App\Http\Controllers\Custom\StoreLocatorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('customer-login',[CustomerLoginController::class,'customerLogin']);
Route::post('send-otp',[CustomerLoginController::class,'sendOTP'])->name('send.otp');
Route::post('verify-otp',[CustomerLoginController::class,'verifyOTP'])->name('verify.otp');


Route::post('update-order-status',[CustomerLoginController::class,'invoiceGenerateExcel'])->name('update-order.status');