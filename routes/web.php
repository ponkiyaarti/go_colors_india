<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CustomerLoginController;
use App\Http\Controllers\Custom\StoreLocatorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test',function(){
    print_r('das');
    exit;
});

Route::post('/getstore-name',[StoreLocatorController::class,'getStoreName'])->name('StoreName.get');

Route::post('/getstore-data',[StoreLocatorController::class,'getStoreDetail'])->name('StoreDetail.get');

Route::get('/getstore-all',[StoreLocatorController::class,'getStoreDetailAll'])->name('StoreDetail.getAll');





?>