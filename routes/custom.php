<?php

use App\Http\Controllers\Custom\StoreLocatorController;


Route::get('test',function(){
	$html = "Gautam";
	return response($html)->withHeaders(['Content-Type' => 'application/json']);
});

Route::get('storelocators/{state_name?}/{city_name?}',[StoreLocatorController::class,'listingNew']);

Route::get('storelocator/{state}/{city}/{slug}','StoreLocatorController@MoreDetail')->name('custom.storeLocator.detail');


?>