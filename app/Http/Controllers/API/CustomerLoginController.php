<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OTPStore;
use App\Http\Traits\CustomerRegisterTrait;
use Validator;
use config;

class CustomerLoginController extends Controller
{
    use CustomerRegisterTrait;


    public function customerLogin()
    {
        return view('customer_login');
    }

    public function sendOTP(Request $request)
    {
        $request_data = $request->all();
        $rules = [
            'mobile_no' => 'required|digits:10',
        ];

        $messages = [
            'mobile_no.required' => 'Enter mobile Number'

        ];

        $validator = \Validator::make($request->all(),$rules,$messages);
        if ($validator->fails()) {
            return response()->json(['success'=>false,'message'=>'Errors',$validator->errors()],400);
        }else{

            $otp = mt_rand(100000, 999999); 
            $msg = "Your otp is ". $otp ." .";
            if(isset($request['mobile_no']))
            {
                $mobile = $request['mobile_no'];
                $otp_url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=".$mobile."&msg=".urlencode($msg)."&msg_type=TEXT&userid=2000198150&auth_schema=plain&password=Gofashion12!&format=text";
                
                $gupshup_response = $this->SendOtpTrait($otp_url);

                if(isset($gupshup_response))
                {
                    $otp_save = new OTPStore;
                    $otp_save->otp = $otp;
                    $otp_save->mobile = $mobile;
                    $otp_save->save();
                }        
                return response()->json(['success' => true, 'message' => 'OTP send in your mobile']);   
            }
        }

    }
    public function verifyOTP(Request $request)
    {   
        $rules = [
            'otp' => 'required'
        ];
        $messages = [
            'otp.required' => 'Enter OTP'
        ];

        $validator = \Validator::make($request->all(),$rules,$messages);
        if ($validator->fails()) {
            return response()->json(['success'=>false,'message'=>'Please Enter Valide OTP']);
        }else{
            if(isset($request['mobile_no']))
            {
                $mobile = $request['mobile_no'];
                $customer_otp = OTPStore::select('otp_id','mobile','otp','created_at')->where('mobile',$mobile)->where('is_verify',1)->orderBy('otp_id', 'DESC')->first();

                $created_at = strtotime($customer_otp['created_at'] . "+1 minutes");
                
                $expiry_date = date('Y-m-d H:i:s',$created_at);
                $current_date = date('Y-m-d H:i:s');

                if($expiry_date > $current_date){
                    if($customer_otp['otp'] == $request['otp']){
                        $is_verify = OTPStore::where('otp_id',$customer_otp['otp_id'])->update(["is_verify" => 0]);
                        return response()->json(['success' => true, 'message' => 'Welcome to go colors india']);   
                    }else{
                        return response()->json(['success' => false, 'message' => 'Please Enter Valide OTP']); 
                    }
                }else{
                    return response()->json(['success' => false, 'message' => 'OTP expire, please Resend']); 
                }
            }
        }
    }

    
}
