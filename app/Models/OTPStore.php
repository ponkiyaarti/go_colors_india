<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OTPStore extends Model
{
    protected $table = 'otp_store';    
    protected $primaryKey = 'otp_id';
    protected $fillable = ['otp_id','otp','mobile','is_verify'];
    // public $timestamps = false;
}
