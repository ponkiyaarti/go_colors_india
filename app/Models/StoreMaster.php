<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreMaster extends Model
{
	protected $table = "store_master";
	protected $primaryKey = "store_master_id";
	protected $fillable = ['store_code','store_latitude','store_longitude','store_time','store_name','store_address','store_country','store_pincode','store_city','store_state','store_phone_number'];


}
